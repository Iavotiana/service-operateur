create database mobilepostgres;

create table operateur(
    idOperateur int not null primary key,
    nom varchar(10),
    prefixe varchar(3),
    mdp varchar(32) not null
);

create sequence seqCompte start 1;
create table compte(
    idCompte int primary key,
    idClient int not null,
    idOperateur int not null,
    num varchar(10),
    mdp varchar(32) not null
);

create sequence seqCredit start 1;
create table credit(
    idCredit int primary key,
    idCompte int not null,
    code varchar(14),
    valeur int,
    daty timestamp
);

create sequence seqToken start 1;
create table token(
    idToken int,
    idCompte int,
    valeur varchar(32),
    daty timestamp,
    statu int check(statu=0 or statu=1)
);

create sequence seqTokenAdmin start 1;
create table tokenAdmin(
    idTokenAdmin int,
    idOperateur int,
    valeur varchar(32),
    daty timestamp,
    statu int check(statu=0 or statu=1)
);

create sequence seqMobileMoney start 1;
create table mobileMoney(
    idMobileMoney int primary key,
    idCompte int not null,
    valeur decimal(12,2),
    daty timestamp,
    statu int check(statu=1 or statu=0)
);

create sequence seqOffre start 1;
create table offre(
    idOffre int primary key,
    idOperateur int not null,
    nom varchar(50) not null,
    code varchar(10) unique,
    prix int not null,
    validite int not null,
    priorite int 
);

create sequence seqDetailsOffreAppel start 1;
create table detailOffreAppel(
    idOAppel int primary key,
    idOffre int not null,
    valeurTTC int not null,
    puMemeOp int not null,
    puAutreOp int not null
);

create sequence seqDetailsOffreInternet start 1;
create table detailOffreInternet(
    idOInternet int primary key,
    idOffre int not null,
    mo int not null,
    types varchar(20)
);

create sequence seqDetailsOffreSms start 1;
create table detailOffreSms(
    idOSms int primary key,
    idOffre int not null,
    nbrSms int not null
);

create table achatOffre(
    idAchatOffre serial primary key,
    idCompte int not null,
    idOffre int not null,
    datyDebut timeStamp,
    datyFin timeStamp,
    appel int,
    sms int,
    internet int
);

create table internet(
    idInternet serial primary key,
    idCompte int not null,
    mo int,
    types varchar(20),
    datyDebut timeStamp,
    datyFin timeStamp
);

create table appel(
    idAppel serial primary key,
    idCompte int not null,
    valeur int,
    datyDebut timeStamp,
    datyFin timeStamp
);


create table sms(
    idSms serial primary key,
    idCompte int not null,
    nbrSms int,
    datyDebut timeStamp,
    datyFin timeStamp 
);

create sequence seqTarif start 1;
create table tarif(
    idTarif int primary key,
    idOperateur int not null,
    puAutreOp real,
    puMemeOp real,
    puMo real,
    puSmsAutreOp real,
    puSmsMemeOp real
);

create table historiqueAppel(
    idHistoAppel serial primary key,
    numNiantso VARCHAR(10),
    numNantsoina VARCHAR(10),
    duree int,
    daty TIMESTAMP
);

alter table compte add constraint FK_compte_idOperateur Foreign key (idOperateur) references operateur(idOperateur);
alter table credit add constraint FK_credit_idCompte Foreign key (idCompte) references compte(idCompte);
alter table token add constraint FK_token_idCompte Foreign key (idCompte) references compte(idCompte);
alter table tokenAdmin add constraint FK_tokenAdmin_idOperateur Foreign key (idOperateur) references operateur(idOperateur);
alter table mobileMoney add constraint FK_mobileMoney_idCompte Foreign key (idCompte) references compte(idCompte);
alter table offre add constraint FK_offre_idOperateur Foreign key (idOperateur) references operateur(idOperateur);
alter table detailOffreAppel add constraint FK_detailOffreAppel_idOperateur Foreign key (idOffre) references offre(idOffre) ON DELETE CASCADE;
alter table detailOffreInternet add constraint FK_detailOffreInternet_idOperateur Foreign key (idOffre) references offre(idOffre) ON DELETE CASCADE;
alter table detailOffreSms add constraint FK_detailOffreSms_idOperateur Foreign key (idOffre) references offre(idOffre) ON DELETE CASCADE;
alter table internet add constraint FK_internet_idCompte Foreign key (idCompte) references compte(idCompte);
alter table appel add constraint FK_appel_idCompte Foreign key (idCompte) references compte(idCompte);
alter table sms add constraint FK_sms_idCompte Foreign key (idCompte) references compte(idCompte);
alter table achatOffre add constraint FK_achatOffre_idCompte Foreign key (idCompte) references compte(idCompte);
alter table achatOffre add constraint FK_achatOffre_idOffre Foreign key (idOffre) references offre(idOffre);

create view v_credit as 
select compte.idCompte,COALESCE(sum(valeur),0)  as valeur ,COALESCE(max(daty),
CURRENT_DATE) as daty from compte left join credit on compte.idCompte=credit.idCompte 
group by compte.idCompte;

create view v_MobileMoney as select COALESCE(idMobileMoney,0) as idMobileMoney, compte.idCompte,compte.idOperateur,compte.num,
COALESCE(valeur,0) as valeur,COALESCE(daty,CURRENT_TIMESTAMP) as daty,COALESCE(statu,1) as statu,
operateur.nom
from compte left join mobileMoney on mobileMoney.idCompte=compte.idCompte
join operateur on compte.idOperateur=operateur.idOperateur order by daty;

create view v_depot_non_valide as 
select * from v_mobileMoney where valeur>0 and statu=0;

create view v_retrait_non_valide as 
select * from v_mobileMoney where valeur<0 and statu=0;

create view v_depot_valide as 
select * from v_mobileMoney where valeur>0 and statu=1;

create view v_retrait_valide as 
select * from v_mobileMoney where valeur<0 and statu=1;

select idCompte,idOperateur,num,sum(valeur) as valeur, max(daty) as daty from v_MobileMoney where idOperateur=3 and statu=1 and daty<'2021-03-20' group by idCompte,idOperateur,num order by valeur;


drop view v_retrait_valide;
drop view v_depot_valide;
drop view v_retrait_non_valide;
drop view v_depot_non_valide;
drop view v_mobileMoney;

create view v_internet as select max(idInternet) as idInternet,idCompte, sum(mo) as mo, 
min(datyDebut) as datyDebut, max(datyFin) as datyFin, internet.types
from internet group by idCompte;

create view v_appel as select max(idAppel) as idAppel, idCompte, sum(valeur) as valeur,
 min(datyDebut) as datyDebut, max(datyFin) as datyFin
 from appel group by idCompte;

create view v_sms as select max(idSms) as idSms, idCompte, sum(nbrSms) as nbrSms,
min(datyDebut) as datyDebut,max(datyFin) as datyFin 
from sms group by idCompte;

select * from internet where idInternet>(select idInternet from internet where  
datyDebut<'2021-03-22 13:00:00' and
'2021-03-22 13:00:00'<datyFin limit 1);

select *from appel where 
datyDebut<to_timestamp('2021-03-28', 'YYYY-MM-DD');

select * from appel where datyDebut<'2021-04-27 07:14:14' and '2021-04-27 07:14:14'<datyFin;

select max(idMobileMoney) as idMobileMoney,idCompte,idOperateur,num,sum(valeur) as valeur, max(daty) as daty,statu,nom from v_MobileMoney where idCompte=3
 and statu=1 group by idCompte,idOperateur,num,statu,nom order by valeur;